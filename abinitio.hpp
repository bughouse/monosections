#ifndef _ABINITIO_HPP_
#define _ABINITIO_HPP_

template <typename T> T sqr(T x) {
    return x*x;
}
enum Gas {
    He, Ar, HeAr
};

template <Gas gas>
struct AbInitioConstants {
    static double Eh, a0;
    static double A, r0, a1, a2, b;
    static double C[17];
};

template <Gas gas> double AbInitioConstants<gas>::Eh = 4.35974417e-18 / 1.3806488e-23 / 300;
template <Gas gas> double AbInitioConstants<gas>::a0 = 5.0066;

template <> double AbInitioConstants<Ar>::A  = 82.9493;
template <> double AbInitioConstants<Ar>::r0 = 6.3745;
template <> double AbInitioConstants<Ar>::a1 = 1.45485;
template <> double AbInitioConstants<Ar>::a2 = 0.0379929;
template <> double AbInitioConstants<Ar>::b  = 1.62365;
template <> double AbInitioConstants<Ar>::C[17]  = {0, 0, 0, 0, 0, 0,
                                               63.7520, 0,
                                               1556.46, 0,
                                               49437.9, 0,
                                               2.07289e6, 0,
                                               1.105297e8, 0,
                                               7.24772e9};

template <> double AbInitioConstants<He>::A  = 6.62002;
template <> double AbInitioConstants<He>::r0 = 5.0066;
template <> double AbInitioConstants<He>::a1 = 1.88553;
template <> double AbInitioConstants<He>::a2 = 0.0639819;
template <> double AbInitioConstants<He>::b  = 1.85822;
template <> double AbInitioConstants<He>::C[17] = {0, 0, 0, 0, 0, 0, 
                                               1.46098, 0,
                                               14.1179, 0,
                                               183.691, 0,
                                               3.26527e3, 0,
                                               7.64399e4, 0,
                                               2.27472e6};

template <> double AbInitioConstants<HeAr>::A  = 23.1693;
template <> double AbInitioConstants<HeAr>::r0 = 5.8921;
template <> double AbInitioConstants<HeAr>::a1 = 1.63329;
template <> double AbInitioConstants<HeAr>::a2 = 0.0462008;
template <> double AbInitioConstants<HeAr>::b  = 1.63719;
template <> double AbInitioConstants<HeAr>::C[17] = {0, 0, 0, 0, 0, 0,
                                                 9.38701, 0,
                                                 165.522, 0,
                                                 3797.16, 0,
                                                 1.16518e5, 0,
                                                 4.66258e6, 0,
                                                 2.36861e8};

template <Gas gas>
struct AbInitio {
    static inline double bMax(const double thmin, const double g)
    {
        const double Eh = AbInitioConstants<gas>::Eh;
        const double C6 = AbInitioConstants<gas>::C[6];
        const double a0 = AbInitioConstants<gas>::a0;
        return std::max(2.0, std::pow(15*Eh*C6*M_PI/thmin/sqr(g), 1./6.) / a0);
    }

    static inline const V2d f(const V2d r)
    {
        const double Eh = AbInitioConstants<gas>::Eh;
        const double A  = AbInitioConstants<gas>::A;
        const double a0 = AbInitioConstants<gas>::a0;
        const double a1 = AbInitioConstants<gas>::a1;
        const double a2 = AbInitioConstants<gas>::a2;

        const double r1 = sqrt(sqr(r)) * a0;

        double f1 = A * a0 * ( - a1 - 2 * a2 * r1) * exp( - a1 * r1 - a2 * r1 * r1);
        double f23 = f_c(r1);

        return (- Eh / r1 * a0 * r) * (f1 + f23);
    }

    static inline double u(const V2d r)
    {
        const double Eh = AbInitioConstants<gas>::Eh;
        const double A  = AbInitioConstants<gas>::A;
        const double a0 = AbInitioConstants<gas>::a0;
        const double a1 = AbInitioConstants<gas>::a1;
        const double a2 = AbInitioConstants<gas>::a2;

        const double r1 = sqrt(sqr(r)) * a0;

        double u1 = A * exp( - a1 * r1 - a2 * r1 * r1);
        double u2 = u_c(r1);

        return Eh * (u1 + u2);
    }

    static inline double f_c(double r1)
    {
        const double a0 = AbInitioConstants<gas>::a0;
        const double b  = AbInitioConstants<gas>::b;
        double* C       = AbInitioConstants<gas>::C;

        double f2  = 0.;
        double f22 = 1.;
        double kf  = 1.;
        double bk  = 1.;
        double r1k = 1.;
        int k = 0;
        while (k < 30) { //(f22 > 1e-15) {
            k  += 1;
            kf *= k;
            bk *= b;
            int nmin = 3;
            int nmax = std::min(8, (k-1) / 2);
            if (nmax >= nmin) {
                double r1knmin = r1k;
                double r12 = r1 * r1;
                f22 = 0.0;
                for (int n = nmin; n <= nmax; n++) {
                    f22 -= 2 * n * C[2*n] * bk * r1knmin / kf;
                    r1knmin /= r12;
                }
                f2 += f22;
                r1k *= r1;
            }
        }

        double f3 = 0.;
        double nf = 1. * 2. * 3. * 4. * 5. * 6.;
        double bn = sqr(b * b * b) * b;
        for (int n = 3; n < 9; n++) {
            f3 += C[2*n] * bn / nf;
            bn *= sqr(b);
            nf *= (2 * n + 1) * (2 * n + 2);
        }

        return - (f2 + f3) * exp(-b*r1) * a0;
    }

    static inline double u_c(double r1)
    {
        const double b  = AbInitioConstants<gas>::b;
        double* C       = AbInitioConstants<gas>::C;

        double s   = 0.;
        double s2  = 1.;
        double kf  = 1.;
        double bk  = 1.;
        double r1k = r1;
        int k = 0;
        while (k < 30) { //(s2 > 1e-15) {
            k  += 1;
            kf *= k;
            bk *= b;
            int nmin = 3;
            int nmax = std::min(8, (k-1) / 2);
            if (nmax >= nmin) {
                double r1knmin = r1k;
                double r12 = r1 * r1;
                s2 = 0.0;
                for (int n = nmin; n <= nmax; n++) {
                    s2 += C[2*n] * bk * r1knmin / kf;
                    r1knmin /= r12;
                }
                s += s2;
                r1k *= r1;
            }
        }
    //    std::cout << "k = " << k << std::endl;
        return - exp(-b*r1) * s;
    }

};

#endif
