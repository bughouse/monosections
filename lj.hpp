#ifndef _LJ_HPP_
#define _LJ_HPP_

struct LJ {
    static inline double bMax(const double thmin, const double g)
    {
        return std::max(2.0, std::pow(2.0*30*M_PI/thmin/sqr(g), 1./6.));
    }

    static inline const V2d f(const V2d r)
    {
        double one_div_r2 = 1. / sqr(r);
        double one_div_r6 = one_div_r2 * one_div_r2 * one_div_r2;
        double tmp = 24*(2 * one_div_r6 * one_div_r6 - one_div_r6)*one_div_r2;
        return tmp * r;
    }

    static inline double u(const V2d r)
    {
        double one_div_r2 = 1. / sqr(r);
        double one_div_r6 = one_div_r2 * one_div_r2 * one_div_r2;
        double tmp = 4*(one_div_r6 - 1.)*one_div_r6;
        return tmp;
    }
};

#endif
