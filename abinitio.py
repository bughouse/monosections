#!/usr/bin/env python

from math import exp, pow, sqrt

pot = {
    'He' : {
        'A'  : 6.62002,
        'r0' : 5.0066,
        'a0' : 0.5291772,
        'a1' : 1.88553,
        'a2' : 0.0639819,
        'b'  : 1.85822,
        'C'  : {
            6  : 1.46098,   8  : 14.1179,   10 : 183.691,
            12 : 3.26527e3, 14 : 7.64399e4, 16 : 2.27472e6
            }
    },
    'Ar' : {
        'A'  : 82.9493,
        'r0' : 6.3745,
        'a0' : 0.5291772,
        'a1' : 1.45485,
        'a2' : 0.0379929,
        'b'  : 1.62365,
        'C'  : {
            6  : 63.7520,   8  : 1556.46,    10 : 49437.9,
            12 : 2.07289e6, 14 : 1.105297e8, 16 : 7.24772e9
            }
    },
    'He-Ar' : {
        'A'  : 23.1693,
        'r0' : 5.8921,
        'a0' : 0.5291772,
        'a1' : 1.63329,
        'a2' : 0.0462008,
        'b'  : 1.63719,
        'C'  : {
            6  : 9.38701,   8  : 165.522,   10 : 3797.16,
            12 : 1.16518e5, 14 : 4.66258e6, 16 : 2.36861e8
        }
    }
}

def u(r, A, a0, a1, a2, b, C, **kwargs):
    r1 = r / a0

    s = 0
    kf = 1.
    bk = 1.
    k  = 0
    ds = 1.
    for k in range(1, 30):
        kf *= k
        bk *= b
        nmin = 3
        nmax = min(8, (k-1) / 2)
        for n in range(nmin, nmax + 1):
            s += C[2*n] * bk * pow(r1, k - 2*n) / kf

    return A * exp(-a1*r1 - a2*r1*r1) - exp(-b*r1) * s

def u_lj(r, e, s):
    return 4 * e * (pow(s/r, 12) - pow(s/r, 6))

n = 1000
xmax = 8.0 
xmin = 0.0

m = 4.3597 / 1.38 * 100000

x  = [ xmin + (i+0.5) / n * (xmax-xmin) for i in range(n) ]

y1 = [ m * u(r, **pot['He']) for r in x ]
y2 = [ m * u(r, **pot['Ar']) for r in x ]
y3 = [ m * u(r, **pot['He-Ar']) for r in x ]


e1 = 10.22
e2 = 124
e12 = sqrt(e1*e2)

d0 = 1.  # 5.0066 * 0.5291772
d1 = 2.575 / d0
d2 = 3.418 / d0
d12 = (d1 + d2) / 2

y4 = [ u_lj(r, e1, d1) for r in x ]
y5 = [ u_lj(r, e2, d2) for r in x ]
y6 = [ u_lj(r, e12, d12) for r in x ]

y1min = min(y1)
y2min = min(y2)
y3min = min(y3)
print y1min, y2min, y3min

ymin = abs(min((y1min, y2min, y3min)))

import pylab as plt

plt.rcParams['font.family'] = 'serif'
fig = plt.figure(figsize=(5, 4))
ax = fig.add_subplot(111)

ax.plot(x, y1, 'b', label='He-He')
ax.plot(x, y3, 'r', label='He-Ar')
ax.plot(x, y2, 'g', label='Ar-Ar')

ax.plot(x, y4, 'b--')
ax.plot(x, y6, 'r--')
ax.plot(x, y5, 'g--')

plt.xlabel(r'$r$, $\AA$')
plt.ylabel(r'$U$, $K$')

plt.ylim( (-1.5*ymin, 3*ymin) )

ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.grid(True)
plt.legend()

plt.tight_layout()

plt.savefig('u_abinitio.eps')
plt.show()

