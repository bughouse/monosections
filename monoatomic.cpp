#include <iostream>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <tuple>
#include <limits>

#include "v.hpp"
#include "runge-kutta.hpp"
#include "abinitio.hpp"
#include "lj.hpp"

const double pi = 4.*std::atan(1.);

inline double error(const X y, const X x) {
    double er = sqr(y.r) / sqr(x.r);
    double ev = sqr(y.v) / sqr(x.v);
    return std::sqrt(er + ev);
}

template <typename Pot>
inline double energy(const X x) {
    return 0.5*sqr(x.v) + Pot::u(x.r);
}

template <typename Pot>
struct G {
    inline const X operator() (const X x) {
        return X(x.v, Pot::f(x.r));
    }
};

template <typename Pot>
std::tuple<X, int, double> calc(X x, const double tol) {

    double time_step = 0.01;
    const double beta = 0.8;

    const double bcut2 = sqr(x.r);
    double min_time_step = std::numeric_limits<double>::max();
    int i = 0;
    while (true) {

        if (min_time_step > time_step)
            min_time_step = time_step;

        const std::pair<X, X> py = RungeKutta<8>::rke(G<Pot>(), time_step, x);

        const X y  = py.first;
        const X ey = py.second;

        double err   = std::max(1e-16, 2*error(ey, x+y));

        double alpha = std::min(4., std::max(0.25, beta*std::pow(tol/err, 1./8.)));
        time_step = time_step * alpha;

        if (err > tol)
            continue;
        
        x = y;

        ++i;

        if (sqr(x.r) > bcut2) 
            return std::make_tuple(x, i, min_time_step);
    }
}

template <typename Pot>
double gbToTheta(double g, V2d r) {
    V2d v(g, 0.);
    X x(r, v);

    X x2;
    int n;
    double min_time_step;
    std::tie(x2, n, min_time_step) = calc<Pot>(x, 1e-12);

    return arg(x2.v);
}


struct Segm {
    double x1, x2;
    double y, y1, y2;
    int m;
    Segm(double x1, double x2, double y1 = 0, double y2 = 0, int m = 0, double y = 0) :
        x1(x1), x2(x2), y(y), y1(y1), y2(y2), m(m) {}
};

class Sigma {
    public:
        Sigma(double x1, double x2) {
            size_t n = 100;
            double dx = (x2 - x1) / n;
            double x3 = x1 + dx;
            for (size_t i = 0; i < n; ++i) {
                s.push_back(Segm(x1, x3));
                x1 += dx;
                x3 += dx;
            }
            k = 0; m = 0; vol = 10000;
            err_sp = -0.002;
        }

        bool add(double x, double y);
    
        void split();

        void write();

        size_t size() const {
            return s.size();
        }

        Segm operator[] (int i) const {
            return s[i];
        }

    private:
        std::vector<Segm> s;
        int k, m, vol;
        double err_sp;

};

bool Sigma::add(double x, double y)
{
    k++;

    size_t l = 0;
    size_t r = s.size() - 1;
    if ( (x < s[l].x1) || (s[r].x2 < x) )
        return true;

    size_t i = 0;
    while (true) {
        i = (l + r)  / 2;
        if (s[i].x1 > x)
            r = i - 1;
        else if (s[i].x2 < x)
            l = i + 1;
        else
            break;
    }

    s[i].y  += y;

    if ( k >= vol ) {
        k = 0;
        m++;
        std::cout << "i = " << m << std::endl;
        split();
        write();
        return false;
    }
    return true;
}

void Sigma::split()
{
    size_t i = 0;
    while ( i < s.size() )
    {
        int m = ++s[i].m;
        double y  = s[i].y / vol;
        double y1 = ( s[i].y1 * (m-1) +     y  ) / m;
        double y2 = ( s[i].y2 * (m-1) + sqr(y) ) / m;

        double err = (m == 1) ?
                     std::numeric_limits<double>::max() : 
                     1. / (m - 1) * (y2 - sqr(y1));
        err = (y1 < 1e-12) ? 
                std::numeric_limits<double>::max() : 
                std::sqrt(err) / y1 ; 

        if (err < err_sp) {
            double x1 = s[i].x1;
            double x2 = s[i].x2;
            double x3 = (x1 + x2) / 2;
            s[i] =                      Segm(x1, x3);
            s.insert(s.begin() + i + 1, Segm(x3, x2));
            ++i;
            std::cout << "split: " << x1 << ' ' << x3 << ' ' << x2 << std::endl;
            std::cout << "m = " << m << ' ' << s[i].m << std::endl;
        }
        else {
            s[i].y  = 0;
            s[i].y1 = y1;
            s[i].y2 = y2;
        }
        ++i;
    }
}

void Sigma::write()
{
    std::ofstream fd("th.out");
    for (size_t i = 0; i < s.size(); ++i) {
//        double x = (s[i].x1 + s[i].x2) / 2;
        double v = s[i].x2 - s[i].x1;
//        fd << x << ' ' << s[i].y1 / v  << std::endl;
        fd << s[i].x1 << ' ' << s[i].y1 / v  << std::endl;
        fd << s[i].x2 << ' ' << s[i].y1 / v  << std::endl;
    }
    fd.close();
}

std::vector<double> make_gs(double gmin, double gmax, int m) {
    std::vector<double> gs;
    gs.reserve(m);
/*
    double dg = gmax / m;
    for (double g = dg / 2; g < gmax; g += dg)
        gs.push_back(g);
*/
    double q = std::exp(std::log(gmax / gmin) / m);
    for (int i = 0; i < m; ++i)
        gs.push_back(gmin * std::pow(q, i));

    return gs;
}

void write_sigma(const std::vector<Sigma>& sigmas, 
                 const std::vector<double>& gs,
                 std::string filename)
{
    std::ofstream fd(filename);
    fd << sigmas.size() << ' ' << sigmas[0].size() << std::endl;
    for (size_t j = 0; j < sigmas.size(); ++j)
        fd << gs[j] << std::endl;
    for (size_t i = 0; i < sigmas[0].size(); ++i) {
        Segm s = sigmas[0][i];
        double x = (s.x1 + s.x2) / 2; 
        fd << x << std::endl;
    }
    for (size_t j = 0; j < sigmas.size(); ++j)
        for (size_t i = 0; i < sigmas[j].size(); ++i) {
            Segm s = sigmas[j][i];
            double v = s.x2 - s.x1;
            fd << s.y1 / v  << std::endl;
        }
    fd.close();
}

template <typename Pot>
void make_stat(const std::string& filename) {
    const double thmin = 0.01 * pi;

    std::vector<double> gs = make_gs(0.3, 60, 60);
    std::vector<Sigma> sigmas(gs.size(), Sigma(thmin, pi));

    size_t j = 0;
    for (;;) 
    {
        double g     = gs[j];
        std::cout << "j, g = " << j << ' ' << g << std::endl;

        g /= sqrt(2);
        const double bmax  = Pot::bMax(thmin, g);
        std::cout << "bmax = " << bmax << std::endl;

        for (int i = 1; ; ++i) {
            double b2 = sqr(bmax) * static_cast<double>(std::rand())/RAND_MAX;    
            double b = std::sqrt(b2);
            V2d r(-std::sqrt(sqr(bmax) - sqr(b)), b);

            double theta = gbToTheta<Pot>(g, r);
            double th = std::abs(theta);

            if (!sigmas[j].add(th, sqr(bmax) / 2 / std::sin(th)))
                break;
        }

        j++;
        if (j == gs.size()) {
            write_sigma(sigmas, gs, filename);
            j = 0;
        }
    }
}

int main(int argc, char * argv[]) {
    if (argc > 1) {
        std::string pot(argv[1]);
        if (pot == "ar_abinitio")
            make_stat< AbInitio<Ar> >("abinitio_Ar.out");
        else if (pot == "he_abinitio")
            make_stat< AbInitio<He> >("abinitio_He.out");
        else if (pot == "he_ar_abinitio")
            make_stat< AbInitio<HeAr> >("abinitio_HeAr.out");
        else if (pot == "lj")
            make_stat< LJ >("lj.out");
    }
}
