#ifndef _X_HPP_
#define  _X_HPP_

struct X {
    V2d r, v;
    X() {}
    X(V2d r, V2d v) :
            r(r), v(v) {}
};

inline std::ostream& operator<<(std::ostream& to, const X x) {
	to << x.r << ' ' << x.v << std::endl;
	return to;
}

inline const X operator+(const X x, const X y) {
    return X(x.r + y.r, x.v + y.v);
}

inline const X operator-(const X x, const X y) {
    return X(x.r - y.r, x.v - y.v);
}

inline const X operator*(double y, const X x) {
    return X(x.r * y, x.v * y);
}

#endif
